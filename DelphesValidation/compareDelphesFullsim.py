import ROOT
from clicpyana.PlottingTools.Plotter import plotComparison, plotComparisonNormalized, createCanvas, obtainPropertySample, obtainPropertyHistogram, setFeaturesAxis
from clicpyana.PlottingTools import GeneralStyle
from clicpyana.ReadingTools import ReaderTools
import sys, os




def ReaderConfigDict( runConfig ):
    configDicts = []
    for readerConfigFile in runConfig["ReaderConfigs"]:
        readerConfigFilePath = runConfig["ConfigDir"] + readerConfigFile
        configDict = ReaderTools.configDictFromJson(readerConfigFilePath  )
        configDicts.append( configDict )
    return configDicts


def CreateCorrectLegendtext( originaltitle):
    if originaltitle.startswith("Delphes"):  return "DELPHES"
    if originaltitle == "9400"   :  return "CLICdet"
    if originaltitle == "2001"   :  return "CLIC_ILD"
    if originaltitle == "6747"   :  return "CLIC_ILD"
    if originaltitle == "2441"   :  return "CLIC_SiD"
    if originaltitle == "2713"   :  return "CLIC_SiD"
    else: return originaltitle


def main(*args):
    if len(args)== 1:
        sys.exit("Please supply a run config, e.g. Configs/runConfig.json")

    nComparisons = len(args) -1
    canv = createCanvas()



    if len(args) > 1:
        runConfigFile = args[1]

    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    


    configFiles = []
    configDicts = ReaderConfigDict( runConfig )

    ##########anadir = runConfig["AnalysisDir"]
    histoconfigDict = ReaderTools.configDictFromJson( runConfig["ConfigDir"] + runConfig["HistoConfig"] )
    plotdir = runConfig["PlotDir"]
    histodict = {}
    labellist = []

    for cfg in configDicts:
        label = cfg["Label"]
        labellist.append(label)
        histoFileName =  cfg["HistoDir"] + "/histo"+label+"_"+runConfig["OverallTag"]+".root"
        print("Accessing Histogram file {}".format(histoFileName))
        if not os.path.exists(histoFileName):
            sys.exit("File {} does not exist".format(histoFileName))
        histofile = ROOT.TFile.Open(histoFileName,"READ")
        #histofile.cd()
        for h in histofile.GetListOfKeys():
            h = h.ReadObj()
            #print h.GetName(), h.GetName().strip("_"+ label)
            #histoname_withoutlabel = h.GetName().strip("_"+ label)
            _histoname = h.GetName().split("_")
            del _histoname[-1]
            histoname_withoutlabel = ("_").join(_histoname)

            if not histoname_withoutlabel in histodict:
                histodict[histoname_withoutlabel] = {}
            histodict[histoname_withoutlabel][label] = h

            histodict[histoname_withoutlabel][label].SetDirectory(0)
            #ROOT.gDirectory.cd()

        histofile.Close()

    #create the histogram to contain the plots
    plotsubdir = plotdir + ("vs").join(labellist) + "_"+ runConfig["OverallTag"]+"/"
    print("Will save the histograms in {}".format(plotsubdir))
    if not os.path.exists(plotsubdir):
        os.mkdir(plotsubdir)
    else:
        print("Overwriting plots in {}".format( plotsubdir ))

    ListOfHistosToPlot= histodict.keys()
    if "Plot_Histos" in runConfig:
        ListOfHistosToPlot = runConfig["Plot_Histos"]

    for histtitle in ListOfHistosToPlot:
        histlist = []
        for l in histodict[histtitle].keys():
            _h = histodict[histtitle][l]
            #print(histtitle, l, configDicts)
            _h.SetLineColor(obtainPropertySample( _h, "Color", configDicts ))
            try:
                _h.Rebin(obtainPropertyHistogram(histtitle, "rebin", histoconfigDict))
            except:
                pass


            _h.GetXaxis().SetRangeUser(obtainPropertyHistogram(histtitle, "xbinlow", histoconfigDict),obtainPropertyHistogram(histtitle, "xbinup", histoconfigDict)  )
            

            #need to set the title (ideally IN the dictionary) to what it is, i.e. Delphes or fullsim
            histoconfigDict[histtitle]["title"] = CreateCorrectLegendtext(_h.GetTitle())
            canv, _h, cliclabel = GeneralStyle.SetGeneralStyle(canv,_h,histoconfigDict[histtitle])


            _h.SetMaximum(1.3* _h.GetMaximum())
            histlist.append(_h)



        if type(histlist[0]) == ROOT.TH1F and not runConfig["comparemode"] == "ratio":
            
            canv,leg = plotComparisonNormalized(canv,histlist, maxfact=1.3)

            canv.Update()
            canv.SaveAs(plotsubdir +histtitle+".pdf"  )
        if type(histlist[0]) == ROOT.TH1F and runConfig["comparemode"] == "ratio":
            #create upper pad
            newcanv = createCanvas("ratio_canvas")
            histpad = ROOT.TPad("histpad","histpad",0,0.4,1,1.)
            histpad.SetBottomMargin(0)
            histpad.SetLeftMargin(0.25)
            

            # dummy again
            histpad.Draw()
            histpad.cd()
            #cd in the upper pad, plot Comparison there
            newcanv,leg = plotComparisonNormalized(newcanv,histlist, maxfact=1.4,WIP=False)

            processname = runConfig["Processname"]
            t1 = GeneralStyle.CreateSmallText(0.09)
            t1.DrawLatexNDC(0.45,0.92, processname  )
            
            leg.SetX1NDC(0.5)
            leg.SetY1NDC(0.58)
            leg.SetX2NDC(0.85)
            leg.SetY2NDC(0.85)

            newcanv.cd()
            #newcanv.Update()
            histpad.cd()
            #adjust the axis to avoid overlap with the ratio axis
            #get the axis and set its labels to 0 size
            oldlabelsize = histlist[0].GetYaxis().GetLabelSize()
            histlist[0].GetYaxis().SetLabelSize(0.)
            ndivisions = 212
#
            histlist[0].GetYaxis().SetNdivisions(ndivisions)
#            
            mini = histlist[0].GetMinimum()
            maxi = histlist[0].GetMaximum()
            xmin = histlist[0].GetXaxis().GetXmin()
            histlist[0].GetXaxis().SetTickLength(0.1)
            myaxis = ROOT.TGaxis( xmin, mini+0.05*maxi, xmin, maxi-0.05*maxi, mini+0.05*maxi, maxi-0.05*maxi, ndivisions, "-"  )
            myaxis = setFeaturesAxis(myaxis, labelsize=oldlabelsize)


           
            binwidth =  histlist[0].GetBinWidth(0)
            if binwidth > 1 and int(binwidth) -binwidth <0.0001:
                binwidth = int(binwidth)
            hasUnitGeV = False
            if "GeV" in histlist[0].GetXaxis().GetTitle():
                hasUnitGeV = True
                print histlist[0].GetXaxis().GetTitle()
                print "has GeV"

            histlist[0].GetXaxis().SetTitle(histoconfigDict[histtitle]["xtitle"]  )
            text = histlist[0].GetXaxis().GetTitle().replace("[GeV]","")

            #"dM_{HH}"
            binwidth = str(binwidth)
            denom = binwidth
            if hasUnitGeV: denom += " GeV"
            unit ="#frac{1}{"+denom+"}"
            #print unit
            
            if histtitle.endswith("_j"):
                #it's an object observable, so the yaxistitle should contain N instead of sigma
                yaxistitle = "{} {} [{}]".format("#frac{1}{N(j)}","#frac{dN(j)}{d"+text+"}",unit)
            elif histtitle.endswith("_mu"):
                yaxistitle = "{} {} [{}]".format("#frac{1}{N(#mu)}","#frac{dN(#mu)}{d"+text+"}",unit)
            elif histtitle.endswith("_e"):
                yaxistitle = "{} {} [{}]".format("#frac{1}{N(e)}","#frac{dN(e)}{d"+text+"}",unit)
            else:
                yaxistitle = "{} {} [{}]".format("#frac{1}{#sigma}","#frac{d#sigma}{d"+text+"}",unit)
            myaxis.SetTitle(yaxistitle)
            myaxis.SetTitleSize(0.09) #modify this if the title is too long. default 0.1
            myaxis.SetTitleOffset(1.05)
            myaxis.SetLabelSize(0.09)
            myaxis.SetLabelFont(42)
            myaxis.SetTitleFont(42)
            myaxis.Draw()
            canv.Modified()
            canv.Update()

            newcanv.cd()
          

            #create the other pad
            ratiopad = ROOT.TPad("ratiopad","ratiopad", 0, 0.01,1, 0.4)
            ratiopad.SetTopMargin(0)
            ratiopad.SetBottomMargin(0.4)
            ratiopad.SetLeftMargin(0.25)
            ratiopad.SetGridy()
            ratiopad.Draw()
            ratiopad.cd()
            #there, plot the ratio
            if len(histlist)==2:
                   rationom = histlist[1].Clone()
                   rationom.Divide(histlist[0])
                   rationom.SetMinimum(0.6)
                   rationom.SetMaximum(1.5-0.01)
                   rationom.SetMarkerColor(ROOT.kBlack)
                   rationom.SetMarkerSize(1.5)
                   rationom.SetMarkerStyle(5)
                   rationom.SetFillColor(ROOT.kOrange-2)
                   rationom.GetXaxis().SetNdivisions(305)
                   rationomxaxis = setFeaturesAxis ( rationom.GetXaxis() ,titlesize= 0.16, titleoffset=0.95,labelsize=0.15)
                   rationom.GetYaxis().SetTitle("")
                   rationom.GetYaxis().SetNdivisions(505)
                   rationom.GetYaxis().SetTitle("#frac{Delphes}{FullSim} ")
                   rationomyaxis = setFeaturesAxis ( rationom.GetYaxis(), titlesize=0.15, titleoffset=0.5,labelsize=0.15)
                   rationom.Draw("E2")
            newcanv.Update()

            GeneralStyle.SaveCanvas( newcanv,rationom, plotsubdir +histtitle+"_ratio.pdf"   )
            GeneralStyle.SaveCanvas( newcanv,rationom, plotsubdir +histtitle+"_ratio.C"   )

        canv.cd()
        if type(histlist[0]) == ROOT.TH2F:
            for l in histodict[histtitle].keys():
                print l
                histodict[histtitle][l].Draw("colz")
                canv.SetBottomMargin(0.22)
                canv.SetLeftMargin(0.25)

                canv.Modified()
                canv.Update()
                GeneralStyle.SaveCanvas( newcanv,rationom, plotsubdir +histtitle+"_"+l+".pdf")

                #GeneralStyle.SaveAs(canv, histodict[histtitle][l], plotsubdir +histtitle+"_"+l+".pdf"  )


    
            

    raw_input("The end")

if __name__ == "__main__":
    main(*sys.argv)
  

