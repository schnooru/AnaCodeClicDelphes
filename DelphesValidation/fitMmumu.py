import ROOT
from clicpyana.PlottingTools.Plotter import plotComparison, createCanvas, obtainPropertySample, fitInvariantMass
from clicpyana.ReadingTools import ReaderTools
import sys, os
from clicpyana.PlottingTools import GeneralStyle

def main(*args):

    nComparisons = len(args) -1
    canv = createCanvas()
    
    anadir = "/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/"

    #runConfigFile = "/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/Configs/runConfig_detVal.json"

    if len(args) > 1:
        runConfigFile = args[1]
    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    
    
    configFiles = []
    configDicts = []

    if len(args)== 1:
        sys.exit("Please supply a run config, e.g. Configs/runConfig.json")
        #configFiles.append("/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/slcioConfig.json")
        #print (" Using default config file(s) {}".format((", ").join(configFiles)))
        #configDicts.append(ReaderTools.configDictFromJson(configFiles[0] ))

    for readerConfigFile in runConfig["ReaderConfigs"]:
        readerConfigFilePath= runConfig["ConfigDir"] + readerConfigFile
        #runOverEventfile(runConfig,readerConfigFilePath)
        configDict=ReaderTools.configDictFromJson(readerConfigFilePath  )
        configDicts.append( configDict )


    plotdir = runConfig["PlotDir"]
    

    histodict = {}
    labellist = []
    #ROOT.gDirectory.cd()
    for cfg in configDicts:
        label = cfg["Label"]
        labellist.append(label)
        histoFileName =  cfg["HistoDir"] + "/histo"+label+"_"+runConfig["OverallTag"]+".root"
        print("Accessing Histogram file {}".format(histoFileName))
        histofile = ROOT.TFile.Open(histoFileName,"READ")
        for h in histofile.GetListOfKeys():
            h = h.ReadObj()
            #print h.GetName(), h.GetName().strip("_"+ label)
            #histoname_withoutlabel = h.GetName().strip("_"+ label)
            _histoname = h.GetName().split("_")
            del _histoname[-1]
            histoname_withoutlabel = ("_").join(_histoname)


            #histodict[h.GetName()] = {}
            if not histoname_withoutlabel in histodict:
                histodict[histoname_withoutlabel] = {}
            histodict[histoname_withoutlabel][label] = h
            #print (h, type(h), type(      histodict[h.GetName()][label]))
            histodict[histoname_withoutlabel][label].SetDirectory(0)
            #ROOT.gDirectory.cd()
        #print histodict
        histofile.Close()

        #print histodict    


    #print histodict

    #create the histogram to contain the plots
    plotsubdir = plotdir + ("vs").join(labellist) + "_"+ runConfig["OverallTag"]+"/"
    print("Will save the histograms in {}".format(plotsubdir))
    if not os.path.exists(plotsubdir):
        os.mkdir(plotsubdir)
    else:
        print("Overwriting plots in {}".format( plotsubdir ))

    histlist = []
    funclist = []
    print histodict["m_mumu_115_135"]
    for label in histodict["m_mumu_115_135"].keys():
        h= histodict["m_mumu_115_135"][label]

        h.SetLineColor(obtainPropertySample( h, "Color", configDicts ))
        h.GetXaxis().SetRangeUser(125,127)
        
        if h.Integral() >0:
            h.Scale(1./h.Integral())
        print ("Fit for {}:".format(label))
        fitfunc, fitresult, h = fitInvariantMass( h)
        
        fitfunc.SetLineColor(h.GetLineColor())
        fitfunc.Draw("l")
        
        print fitfunc.GetParameter(0)

        histoconfigDict = ReaderTools.configDictFromJson( runConfig["ConfigDir"] + runConfig["HistoConfig"] )
        canv.Modified()
        canv.Update()

        if h.GetTitle() == "2001":
            h.SetTitle("CLIC_ILD")
        if h.GetTitle() == "Delphes-1999":
            h.SetTitle("DELPHES")
        if h.GetTitle() == "9400":
            h.SetTitle("CLICdet")
        if h.GetTitle() == "Delphes":
            h.SetTitle("DELPHES")

        histlist.append(h)
        funclist.append(fitfunc)
        #fitfile = open(plotsubdir + label + "_Mmumu_fit.txt",'w')
        #print("this is the text {}".format(fitresult.Write()))
        #fitfile.write(fitresult.Print())
        #fitfile.close()
        #h.Draw()
        #raw_input()
    canv,leg = plotComparison(canv,histlist,maxfact=1.3,legendymin=0.7,textsize=0.065,DoNormalize=False,WIP=False) 
    for h in histlist:
        h.GetYaxis().SetRangeUser(0,0.5)




        print histoconfigDict
        canv, h, cliclabel = GeneralStyle.SetGeneralStyle(canv,h, histoconfigDict["m_mumu_115_135"])
        h.GetYaxis().SetTitle("#frac{1}{#sigma} #frac{d#sigma}{dm_{#mu#mu}} [#frac{1}{0.2GeV}]")
        #h.GetYaxis().SetTitleOffset(0.5)
        h.GetYaxis().SetTitleSize(0.065)
        h.GetXaxis().SetTitleSize(0.075)

    processname = runConfig["Processname"]
    t1 = GeneralStyle.CreateSmallText(0.065)
    t1.DrawLatexNDC(0.45,0.92, processname  )

    for f in funclist:
        f.Draw("same")
        

        canv.SetLeftMargin(0.23)
        canv.Modified()
        canv.Update()
    
    raw_input("Save? (enter)")

    canv.SaveAs(plotsubdir +"m_Hmumu_fit.pdf" )
    canv.SaveAs(plotsubdir +"m_Hmumu_fit.C" )

    
            

    raw_input()

if __name__ == "__main__":
    main(*sys.argv)
  

