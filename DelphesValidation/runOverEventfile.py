import ROOT
from math import log10
import sys, os

from clicpyana.ReadingTools.ReadDelphes import ReadDelphes
from clicpyana.ReadingTools.ReadSlcio import ReadSlcio
from clicpyana.AnalysisTools.HzqqEvent import HzqqEvent
from clicpyana.AnalysisTools.HistogramCreator import HistogramCreator
from clicpyana.AnalysisTools.Selection import Selection
from clicpyana.PlottingTools.Plotter import plotComparisonNormalized, createCanvas
from clicpyana.ReadingTools import ReaderTools
from clicpyana.AnalysisTools.FillerHistos import FillerHistos

def runOverEventfile(runConfig,readerConfig,selectionConfig, histoConfig):
    #config = reader configs

    #config =  ReaderTools.configDictFromJson(readerConfigFile)
    config = readerConfig
    anadir = runConfig["AnalysisDir"]#"/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/"


    label = config["Label"]


    #canv = createCanvas()

    outputDir = config["HistoDir"]
    if config["Delphes"]:
        eventReader = ReadDelphes(readerConfig)
    else:
        eventReader = ReadSlcio(readerConfig)
    Histos = HistogramCreator(histoConfig  , label).histsdict

    nentries = eventReader.nentries
    #nevent = 0
    for nevent,event in enumerate(eventReader.readerLoop()):
        #nevent += 1
        if nevent % 500 == 0:
            print( "Reading event number {} of {}".format(nevent, eventReader.nentries ))
        if nevent == eventReader.lastEvent:
            break
        if readerConfig["Analysis"] == "HZqq":
            AnalysisEvent = eventReader.FillHzqqEvent(event)
        elif readerConfig["Analysis"] == "WW":
            AnalysisEvent = eventReader.FillWWEvent(event)
        elif readerConfig["Analysis"] == "ttH":
            AnalysisEvent = eventReader.FillTTHEvent(event)
        elif readerConfig["Analysis"] == "Hmumu":
            AnalysisEvent = eventReader.FillHmumuEvent(event)
            
        if not Selection( selectionConfig ).passSelection(AnalysisEvent):
            continue

        Histos = FillerHistos(readerConfig,AnalysisEvent,Histos)



    
    histoFile = ROOT.TFile.Open(outputDir + "/histo"+label+"_"+runConfig["OverallTag"]+".root","RECREATE")

    for h in Histos.keys():
        Histos[h].Write()
    print("Saving the histograms in {}".format(histoFile.GetPath()).strip(":/"))
    histoFile.Close()

def main(*args):
    ROOT.gStyle.SetOptStat(0)

    runConfigFile = "/home/schnooru/Projects/Delphes/CLICdet-Delphes/MyAnalysisCode/Configs/runConfig_detVal.json"
    if len(args) > 1:
        runConfigFile = args[1]
    runConfig = ReaderTools.configDictFromJson(runConfigFile)
    
    for readerConfigFile in runConfig["ReaderConfigs"]:
        readerConfigFilePath= runConfig["ConfigDir"] + readerConfigFile
        readerConfig =  ReaderTools.configDictFromJson(readerConfigFilePath)
        selectionConfig = ReaderTools.configDictFromJson(runConfig["ConfigDir"] +runConfig["SelectionConfig"])
        histoConfig =  ReaderTools.configDictFromJson(runConfig["ConfigDir"]+runConfig["HistoConfig"])
        runOverEventfile(runConfig,readerConfig,selectionConfig, histoConfig)
    

if __name__ == "__main__":
    main(*sys.argv)
  