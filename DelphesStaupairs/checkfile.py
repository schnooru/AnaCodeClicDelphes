import ROOT
import sys, os



from ROOT import TH1F, TCanvas, TLorentzVector

def main(*args):
    if len(args)<2:
        sys.exit("Usage: please provide the name of the input file as argument to the script")
    delphesDir = "/home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes/"
        
    loading = ROOT.gSystem.Load(delphesDir+"libDelphes")
    if not loading == 0 or loading == 1:
        print("TSystem::Load has returned {}".format(loading))
        print("Probably could not find libDelphes. Put it in PATH or execute this in the delphes working directory")
        sys.exit()
    try:
        ROOT.gInterpreter.Declare('#include "'+delphesDir+'classes/DelphesClasses.h"')
        ROOT.gInterpreter.Declare('#include "'+delphesDir+'external/ExRootAnalysis/ExRootTreeReader.h"')
    except AttributeError:
        print ("Problem: was not able to find the necessary Delphes headers.")
        pass


    chain = ROOT.TChain("Delphes")
    inputfile = args[1]
    chain.Add(inputfile)
    treereader = ROOT.ExRootTreeReader(chain)   

    branchDict = {}
    branchDict["truthparticles"] = treereader.UseBranch("Particle")
    branchDict["VLCjetR10N4"] = treereader.UseBranch("VLCjetR10N4")

    nentries = treereader.GetEntries()
    nevent = 0
    h = TH1F("h","h;pT(tau)[GeV]",100,0,1500)
    c = TCanvas("canv","canv",800,600)
    for event in range(0,nentries):
        nevent += 1
        if nevent % 200 == 0:
            print("Reading event n. {}".format(nevent))
        treereader.ReadEntry(event)

        nlep = 0
        #filter semileptonic decays
        for p in branchDict["truthparticles"]:

            #find the tau
            if abs(p.PID)==15:
                h.Fill(p.PT)
                    

    h.Draw("E1")
    c.Modified()                
    c.Update()
    save_as = raw_input("Save as? ")
    c.SaveAs(save_as)

        

if __name__ == "__main__":
    main(*sys.argv)
  