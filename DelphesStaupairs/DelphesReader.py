import ROOT
import sys

class DelphesReader:
    """Creates a reader object for Delphes """

    
    def __init__(self, input_file):
        ##self.initialize_delphes()
        self.chain = ROOT.TChain("Delphes")
        print("Reading input file {}".format(input_file))
        self.chain.Add(input_file)
        self.treereader = ROOT.ExRootTreeReader(self.chain)
        self.nevents = self.treereader.GetEntries()
        return
    def close_chain(self):
        self.treereader.Clear()
        self.chain.Clear()
        del self.treereader
        del self.chain
        return
    def initialize_delphes(self,
                               delphes_dir="/home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes/"):

#        if delphes_dir+"libDelphes.so" in ROOT.gSystem.ListLibraries()

        print("loading "+delphes_dir+"libDelphes")
        loading = ROOT.gSystem.Load(delphes_dir+"libDelphes")
        if not loading == 0 or loading == 1:

            print("TSystem::Load has returned {}".format(loading))
            print("Probably could not find libDelphes. Put it in PATH or execute this in the delphes working directory")
            print("e.g.  /home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes")
            sys.exit()
        try:
            ROOT.gInterpreter.Declare('#include "'+delphes_dir+'classes/DelphesClasses.h"')
            ROOT.gInterpreter.Declare('#include "'+delphes_dir+'external/ExRootAnalysis/ExRootTreeReader.h"')
        except AttributeError:
            print ("Problem: was not able to find the necessary Delphes headers.")
            pass
        return


    def initialize_delphes_branches_dict(self,
                                  branch_names=["Particle"]):
        branchdict = {}
        for br_ in branch_names:
            branchdict[br_] = self.treereader.UseBranch(br_)
        return branchdict

        
