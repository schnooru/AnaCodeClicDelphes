from ROOT import TLorentzVector

class StauEvent:
    """A class of Stau pair event properties"""

    def __init__(self, delphes_reader, nevent, branches_dict):
        if not delphes_reader:
            print("Problem: the delphes reader is not defined")
        self.delphes_reader = delphes_reader
        self.delphes_reader.treereader.ReadEntry(nevent) #loads the event
        self.branches_dict = branches_dict
        return

    def get_branch(self, branchname):
        try:
            jetbranch = self.branches_dict.get(branchname)
        except KeyError as e: 
            print("{} => this branch needs to be initialized using initialize_delphes_branches_dict".format(e))
        return jetbranch
            
    def taucuts(self):
        jetbranch = self.get_branch("VLCjetR07N2")
        if not len(jetbranch) == 2:
            return 0
        for j in jetbranch:
            j_fourvec = j.P4()
            if not j.TauTag or not j.PT > 30 or not j_fourvec.Theta() > 0.26 or not j_fourvec.Theta() < 2.87:
            #if not j.TauTag or not j_fourvec.Theta() > 0.26 or not j_fourvec.Theta() < 2.87:
                return 0
        return 1

        
    def di_tau_properties(self):
        jetbranch = self.get_branch("VLCjetR07N2")
        m_tt = -999
        D_phi_tt = -999
        tau1, tau2 = TLorentzVector(), TLorentzVector()
        if not len(jetbranch) == 2:
            return {"m_tt" : m_tt, "D_phi_tt": D_phi_tt }
        if not jetbranch[0] or not jetbranch[1]:
            return {"m_tt" : m_tt, "D_phi_tt": D_phi_tt }
        if not jetbranch[0].TauTag or not jetbranch[1].TauTag:
            return {"m_tt" : m_tt, "D_phi_tt": D_phi_tt }
       
            
        tau1 = jetbranch[0].P4()
        tau2 = jetbranch[1].P4()
               
       
        m_tt = (tau1+tau2).P()
        #print(m_tt)    
        D_phi_tt = tau1.DeltaPhi(tau2)
        theta_tt = (tau1+tau2).Theta()
        #return m_tt
        return {"m_tt" : m_tt, "D_phi_tt": D_phi_tt, "theta_tt" :theta_tt }

        

        
    def tau1_pt(self):
        jetbranch = self.get_branch("VLCjetR07N2")
        pt_j1 = -999
        for j in jetbranch:
            if not j.TauTag: continue
            if j.PT > pt_j1:
                pt_j1 = j.PT
        return pt_j1
                

    def tau_pt(self):
        jetbranch = self.get_branch("VLCjetR07N2")
        pt_j = -999
        for j in jetbranch:
            if not j.TauTag: continue
            pt_j = j.PT
        return pt_j

    def tau_pt_truth(self):
        try:
            mcpartbranch = self.branches_dict.get("Particle")
        except KeyError as e: 
            print("{} => this branch needs to be initialized using initialize_delphes_branches_dict".format(e))
        pt_tau_truth = []
        for t_ in mcpartbranch:
            if abs(t_.PID)==15:
                pt_tau_truth.append(t_.PT)
            
        return pt_tau_truth

        
    def n_tau_tags(self):
        jetbranch = self.get_branch("VLCjetR07N2")
        n_tau_tags = 0
        for j in jetbranch:
            #print j.TauTag
            if j.TauTag:
                n_tau_tags += 1
        return n_tau_tags
  
    