import ROOT
from clicpyana.ReadingTools import ReaderTools
from clicpyana.PlottingTools import GeneralStyle
from clicpyana.PlottingTools.Plotter import plotComparison, plotComparisonNormalized, createCanvas, obtainPropertySample, obtainPropertyHistogram, setFeaturesAxis
import sys, os
import DelphesReader
import StauEvent
from ROOT import gStyle, gROOT
import math
import csv



    
def apply_cuts( reader,branches_dict, stau_type ):
    n_after_cuts = 0
    for event in range(0,reader.nevents):
        if event % 1000 == 0:
            print("Running over event no {}".format(event))
        stau_event = StauEvent.StauEvent( reader , event,branches_dict )
        ditau_properties = stau_event.di_tau_properties()

        if ditau_properties["m_tt"] <= 0 or ditau_properties["D_phi_tt"] <= 0:
            continue


        if stau_type == "stau1":
            dphi_min = 1.0
            dphi_max = 3.1
            theta_cut = 0.8
        else:
            dphi_min = 2.0
            dphi_max = 3.1
            theta_cut = 0.5
        #apply cuts
        if not stau_event.taucuts():
            continue
        if not ditau_properties["D_phi_tt"] > dphi_min or not ditau_properties["D_phi_tt"] < dphi_max:
            continue
        if not ditau_properties["theta_tt"] > theta_cut or not  ditau_properties["theta_tt"] < math.pi - theta_cut:
            continue
        if not ditau_properties["m_tt"] > 200:
            continue
        n_after_cuts += 1

    eff = float(n_after_cuts)/reader.nevents
    print("Events after cuts: {}, Efficiency of cuts: {}".format(n_after_cuts,eff))
    

    nevent_dict = {}
    nevent_dict["total"] = reader.nevents
    nevent_dict["after_cuts"] = n_after_cuts

    del ditau_properties
    del stau_event

    return nevent_dict

def plot_histograms( reader, branches_dict, canvas ,observable="theta_tt" ):
    histos = {}
    histos["n_tau_tag"]       = ROOT.TH1F("h",";n(#tau tags);fraction",5,0,5)
    histos["tau_pt"]          = ROOT.TH1F("h",";p_T(#tau tagged) ;",50,0,1000)
    histos["tau_pt_truth"]    = ROOT.TH1F("h",";p_T(#tau truth) ;",50,0,1500)
    histos["Delta_phi_tt"]    = ROOT.TH1F("h_dphi",";#Delta #phi (#tau #tau);" , 20,0,math.pi)
    histos["M_tt"]            = ROOT.TH1F("h_mtt",";M(#tau#tau) [GeV];",100,0,1500 )
    histos["theta_tt"]        = ROOT.TH1F("h_theta_tt",";#theta(#tau#tau) [GeV];",50,0, math.pi )

    
    n_after_cuts = 0
    for event in range(0,reader.nevents):
        
        if event % 1000 == 0:
            print("Running over event no {}".format(event))
        #if event == 10:
        #    break

        stau_event = StauEvent.StauEvent( reader , event,branches_dict )
        ditau_properties = stau_event.di_tau_properties()

        if ditau_properties["m_tt"] <= 0 or ditau_properties["D_phi_tt"] <= 0:
            continue

        #apply cuts
        if not stau_event.taucuts():
            continue
        #if not ditau_properties["D_phi_tt"] < 3.0:
        #    continue
        #if not ditau_properties["theta_tt"] > 0.8 or not  ditau_properties["theta_tt"] < math.pi - 0.8:
        #    continue
        #if not ditau_properties["m_tt"] < 1000:
        #    continue

        n_after_cuts += 1
        n_tau_tags = stau_event.n_tau_tags()
        histos["n_tau_tag"].Fill(n_tau_tags)

        histos["M_tt"].Fill(ditau_properties["m_tt"])
        histos["Delta_phi_tt"].Fill(ditau_properties["D_phi_tt"])
        if ditau_properties.get("m_tt"):
            histos["theta_tt"].Fill(ditau_properties.get("theta_tt"))
        pt_tau_tags = stau_event.tau_pt()
        histos["tau_pt"].Fill(pt_tau_tags)
        tau_pt_truth = stau_event.tau_pt_truth()
        for p in tau_pt_truth:
            histos["tau_pt_truth"].Fill(p)


    eff = float(n_after_cuts)/reader.nevents
    print("Events after cuts: {}, Efficiency of cuts: {}".format(n_after_cuts,eff))
    

    nevent_dict = {}
    nevent_dict["total"] = reader.nevents
    nevent_dict["after_cuts"] = n_after_cuts
    histos["n_tau_tag"].Scale(1./reader.nevents)
    histos["n_tau_tag"].GetYaxis().SetTitleOffset(1.4)
    histos["n_tau_tag"].GetXaxis().SetNdivisions(005)


    histos[observable].Draw("E1")
    
    canvas.Modified()
    canvas.Update()
    raw_input()
    
    return canvas, histos[observable], nevent_dict


def initialize_delphes(delphes_dir="/home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes/"):
    
    #        if delphes_dir+"libDelphes.so" in ROOT.gSystem.ListLibraries()
    
    
    print("loading "+delphes_dir+"libDelphes")
    loading = ROOT.gSystem.Load(delphes_dir+"libDelphes")
    if not loading == 0 or loading == 1:
        
        print("TSystem::Load has returned {}".format(loading))
        print("Probably could not find libDelphes. Put it in PATH or execute this in the delphes working directory")
        print("e.g.  /home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes")
        sys.exit()
    try:
        ROOT.gInterpreter.Declare('#include "'+delphes_dir+'classes/DelphesClasses.h"')
        ROOT.gInterpreter.Declare('#include "'+delphes_dir+'external/ExRootAnalysis/ExRootTreeReader.h"')
    except AttributeError:
        print ("Problem: was not able to find the necessary Delphes headers.")
        pass
    return


def plot_three_examples():
    canvas = createCanvas("canv",600,500)

    gStyle.SetOptStat(0)
    gStyle.SetLineWidth(2)
    gStyle.SetTitleFont(42)
    gStyle.SetLabelFont(42)
    gStyle.SetPadTickX(1)
    gStyle.SetPadTickY(1)
    gStyle.SetLabelSize(0.04)
    gROOT.ForceStyle()
    canvas.UseCurrentStyle()
    canvas.SetBottomMargin(0.17)
    canvas.SetLeftMargin(0.17)
    
    xsec_table = "xsec_stau2_neutralino2.txt"
    xsec_table = "xsec_stau1_neutralino1.txt"

#    delphes_dir="/home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes/"
#    print("loading "+delphes_dir+"libDelphes")
#    loading = ROOT.gSystem.Load(delphes_dir+"libDelphes")
#    if not loading == 0 or loading == 1:
#
#        print("TSystem::Load has returned {}".format(loading))
#        print("Probably could not find libDelphes. Put it in PATH or execute this in the delphes working directory")
#        print("e.g.  /home/schnooru/CLIC/Projects/Delphes/Delphes_VLC/delphes")
#        sys.exit()
#
#    try:
#        ROOT.gInterpreter.Declare('#include "'+delphes_dir+'classes/DelphesClasses.h"')
#        ROOT.gInterpreter.Declare('#include "'+delphes_dir+'external/ExRootAnalysis/ExRootTreeReader.h"')
#    except AttributeError:
#        print ("Problem: was not able to find the necessary Delphes headers.")
#        pass
#    return

    initialize_delphes()
    plot = "n_tau_tag"
    plot = "tau_pt"
    #plot = "Delta_phi_tt"
    plot = "M_tt"
    plot = "theta_tt"
    

    ################################
    ##optimize stau2
    stau_type = "2"
    reader_sig1 = DelphesReader.DelphesReader("/eos/home-s/schnooru/Data_CLIC/stau_pairs_4/staupairs_mstau{}_450_mneu_448_polm80.root".format(stau_type))
    stau_branches_dict = reader_sig1.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    n_event_dict_sig1 = apply_cuts(reader_sig1,stau_branches_dict, stau_type )
    reader_sig2 = DelphesReader.DelphesReader("/eos/home-s/schnooru/Data_CLIC/stau_pairs_4/staupairs_mstau{}_450_mneu_448_polp80.root".format(stau_type))
    stau_branches_dict = reader_sig2.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    n_event_dict_sig2 = apply_cuts(reader_sig2,stau_branches_dict , stau_type)
    ##canvas, histo, nevent_dict = plot_histograms(reader_sig2,stau_branches_dict, canvas ,plot)
    xsec_polp80 = 4.4570923
    xsec_polm80 = 1.5591864
    ################################
    ###############################
    #optimize stau1
    #stau_type = "1"
    #reader_sig1 = DelphesReader.DelphesReader("/eos/home-s/schnooru/Data_CLIC/stau_pairs_4/staupairs_mstau{}_1100_mneu_1098_polm80.root".format(stau_type))
    #stau_branches_dict = reader_sig1.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    #n_event_dict_sig1 = apply_cuts(reader_sig1,stau_branches_dict, stau_type )
    #reader_sig2 = DelphesReader.DelphesReader("/eos/home-s/schnooru/Data_CLIC/stau_pairs_4/staupairs_mstau{}_1100_mneu_1098_polp80.root".format(stau_type))
    #stau_branches_dict = reader_sig2.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    #n_event_dict_sig2 = apply_cuts(reader_sig2,stau_branches_dict , stau_type)
    ##canvas, histo, nevent_dict = plot_histograms(reader_sig2,stau_branches_dict, canvas ,plot)
    ################################

    
    #reader_sig2 = DelphesReader.DelphesReader("/eos/user/s/schnooru/Data_CLIC/stau_pairs/staupairs_mstau1_800_mneu_98_polm80.root")
    #stau_branches_dict = reader_sig2.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    #n_event_dict_sig2 = apply_cuts(reader_sig2,stau_branches_dict )
    ##canvas, histo, nevent_dict = plot_histograms(reader_sig2,stau_branches_dict, canvas  ,plot)
    ##canvas.SaveAs("staupairs_mstau1_800_mneu_98_polm80_{}.pdf".format(plot))
    

    reader_bgp = DelphesReader.DelphesReader("/eos/home-s/schnooru/Data_CLIC/tau_pairs/taupairs_all_polp80.root")
    stau_branches_dict = reader_bgp.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    n_event_dict_bgp = apply_cuts(reader_bgp,stau_branches_dict , stau_type)


    reader_bgm = DelphesReader.DelphesReader("/eos/home-s/schnooru/Data_CLIC/tau_pairs/taupairs_all_polm80.root")
    stau_branches_dict = reader_bgm.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    n_event_dict_bgm = apply_cuts(reader_bgm,stau_branches_dict , stau_type)


    
#    canvas, histo, nevent_dict = plot_histograms(reader_bgm,stau_branches_dict, canvas ,plot)
    #canvas.SaveAs("taupairs_polm80_{}.pdf".format(plot))
    
    print("Significances with polarisation scheme:")
    S=4000*float(n_event_dict_sig1["after_cuts"])/float(n_event_dict_sig1["total"])*xsec_polm80 + 1000* float(n_event_dict_sig2["after_cuts"])/float(n_event_dict_sig2["total"])*xsec_polp80
    B=4000*float(n_event_dict_bgm["after_cuts"])/float(n_event_dict_bgm["total"])*216.37 + 1000* float(n_event_dict_bgp["after_cuts"])/float(n_event_dict_bgp["total"])*25.11
    print(S,float(n_event_dict_sig1["after_cuts"])/float(n_event_dict_sig1["total"]),"1.5591864"  ,float(n_event_dict_sig2["after_cuts"])/float(n_event_dict_sig2["total"]),"4.4570923 ")
    print(B,  float(n_event_dict_bgm["after_cuts"])/float(n_event_dict_bgm["total"]),216.37, float(n_event_dict_bgp["after_cuts"])/float(n_event_dict_bgp["total"]),25.11 )
    print(S/math.sqrt(S+B))
    
def xsec_dict_from_table(xsec_table_file):
    xsec_dict = {}
    csvfile = open(xsec_table_file,'rb')

    reader = csv.reader(csvfile)
    for line in reader:
        line = line[0].split(" ")
        if line[0].startswith("#"): continue
        #print line
        mstau = line[0]
        mneu = line[1]
        pol = line[2]
        case = "{}_{}_{}".format(mstau,mneu,pol)
        xsec = line[3]
        xsec_dict[case] = float(xsec)

    return xsec_dict


def write_table_of_significances(proc_dict,table_file,xsec_dict, stau_type):
    print("Creating table file {}".format(table_file))
    text = "#mstau [GeV], mneu [GeV], efficiency(P=-80%), efficiency(P=+80%), sigma[fb](P=-80%), sigma[fb](P=+80%), s/sqrt(s+b) for 4:1 pol scheme, s/sqrt(s+b) for no beam polarisation\n"



#    if polarisation == "polm80":
#        BG = 5000*25.1*0.0103
#    else:
    #        BG = 5000*3.165*0.01314
    
    #this is only for tauneutrinos!! -> change it!
    #BG = 4000*25.1*0.0103 + 1000*3.165*0.01314 #this is only for tauneutrinos!! -> change it!
    #BG_nopol = 2500*25.1*0.0103 + 2500*3.165*0.01314

    #BG = 4000*216.37* 0.02478 + 1000*25.11*0.02308
    #BG_nopol =  2500*216.37* 0.02478 + 2500*25.11*0.02308
    if stau_type == "stau1":
        BG = 4000*216.37* 0.00016 + 1000*25.11*0.0002
        BG_nopol =  2500*216.37* 0.00016 + 2500*25.11*0.0002
    if stau_type == "stau2":
        BG = 4000*216.37*6e-05  + 1000*25.11*0.00014
        BG_nopol =  2500*216.37*6e-05 + 2500*25.11*0.00014
            
    print proc_dict
    #print xsec_dict
    for p in proc_dict:
        mstau = p.split("_")[2]
        mneu = p.split("_")[4]
        if proc_dict[p] == {}:
            continue
        if "polp80" in p:
            efficiency_polp80 = float(proc_dict[p]["after_cuts"])/float(proc_dict[p]["total"])
            case = "{}_{}_polp80".format(mstau,mneu)
            xsec_polp80 = xsec_dict[case]
            print(p)
            p = p.replace("polp80","polm80")
            print(p)
            efficiency_polm80 = float(proc_dict[p]["after_cuts"])/float(proc_dict[p]["total"])
            case = "{}_{}_polm80".format(mstau,mneu)
            xsec_polm80 = xsec_dict[case]
            
        #if "polm80" in p:
        #    efficiency_polm80 = float(proc_dict[p]["after_cuts"])/float(proc_dict[p]["total"])
        #    case = "{}_{}_polm80".format(mstau,mneu)
                    
        
            S = 4000 * xsec_polm80 * efficiency_polm80 + 1000 * xsec_polp80 * efficiency_polp80
            B = BG  #hardcoded
            significance = S/math.sqrt(S+B)
            S = 2500 * xsec_polm80 * efficiency_polm80 + 2500 * xsec_polp80 * efficiency_polp80
            significance_nopol = S/math.sqrt(S+BG_nopol)
            text += "{} {} {} {} {} {} {} {}\n".format(mstau,mneu, efficiency_polm80, efficiency_polp80, xsec_polm80, xsec_polp80, significance, significance_nopol)

    f = open(table_file, "w")
    f.write(text)
    f.close()
    print("wrote significances to {}".format(table_file))
    return

def event_yield_dict(proc_ntuple, stau_type):
    

    
    if "polp80" in proc_ntuple:
        polarisation = "polp80"
    if "polm80" in proc_ntuple:
        polarisation = "polm80"

    proc_name = proc_ntuple.split("/")[-1].split(".")[0]+"_"+polarisation
    print("Proc_ntuple{}".format(proc_ntuple))
    reader = DelphesReader.DelphesReader(proc_ntuple)
    
    stau_branches_dict = reader.initialize_delphes_branches_dict( ["VLCjetR07N2","VLCjetR07N2","Particle"]  )
    nevent_dict = apply_cuts(reader,stau_branches_dict , stau_type)
    #proc_dict[proc_name] = nevent_dict
    #print proc_dict
    reader.close_chain()


    del reader
    del stau_branches_dict
    #del nevent_dict
    #del proc_dict
    #print proc_dict[proc_name]
    return nevent_dict
def analyze_all(stau_type,delphes_input_dir, output_table_file_name):
    
    initialize_delphes()
    
    is_verbose = False

    delphes_outputs =ReaderTools.walkInputDir(delphes_input_dir,".root",is_verbose)
    
    #xsec_table = "xsec_{}_neutralino{}.txt".format(stau_type, stau_type[-1])
    xsec_table = "{}_neutralino{}_xsecs.txt".format(stau_type, stau_type[-1])
    xsec_dict = xsec_dict_from_table(xsec_table)
    print("Running on {}".format(stau_type))
    print("Using cross sections from {}".format(xsec_table))
    #######delphes_outputs = [ '/eos/user/s/schnooru/Data_CLIC/stau_pairs//staupairs_mstau2_500_mneu_198_polm80.root', '/eos/user/s/schnooru/Data_CLIC/stau_pairs//staupairs_mstau2_500_mneu_198_polp80.root']

    proc_dict = {}
    n=0

    for proc_ntuple in delphes_outputs:
        if "polp80" in proc_ntuple:
            polarisation = "polp80"
        if "polm80" in proc_ntuple:
            polarisation = "polm80"
        if stau_type not in proc_ntuple: continue
        n+=1
          
        proc_name = proc_ntuple.split("/")[-1].split(".")[0]  #+"_"+polarisation
        proc_dict[proc_name] = {}
        #if n > 3:
        #    break
    n=0
    for proc_ntuple in delphes_outputs:

        if stau_type not in proc_ntuple: continue
        n+=1
        proc_name = proc_ntuple.split("/")[-1].split(".")[0]  #+"_"+polarisation
        proc_dict[proc_name] = event_yield_dict(proc_ntuple, stau_type) #,proc_dict, stau_type)
        #print(proc_dict[proc_ntuple])
        #print(sys.getsizeof(proc_dict))
        #if n > 3:
        #    break
    #print proc_dict

    table_file = output_table_file_name
    write_table_of_significances(proc_dict,table_file,xsec_dict, stau_type)
    return


def write_significances_standalone(efficiency_xsec_tables,table_file, stau_type):

    print("Creating table file {}".format(table_file))
    text = "#mstau [GeV], mneu [GeV], efficiency(P=-80%), efficiency(P=+80%), sigma[fb](P=-80%), sigma[fb](P=+80%), s/sqrt(s+b) for 4:1 pol scheme, s/sqrt(s+b) for no beam polarisation\n"

    if stau_type == "stau1":
        BG = 4000*216.37* 0.00016 + 1000*25.11*0.0002
        BG_nopol =  2500*216.37* 0.00016 + 2500*25.11*0.0002
    if stau_type == "stau2":
        BG = 4000*216.37*6e-05  + 1000*25.11*0.00014
        BG_nopol =  2500*216.37*6e-05 + 2500*25.11*0.00014
    tfr = open(efficiency_xsec_tables)

    for line in tfr:
        if line.startswith("#"):
            continue
        #print line
        mstau = line.split(" ")[0]
        mneu = line.split(" ")[1]
        efficiency_polm80 = float(line.split(" ")[2])
        efficiency_polp80 = float(line.split(" ")[3])
        sigma_polm80 = float(line.split(" ")[4])
        sigma_polp80 = float(line.split(" ")[5])
        S_polscheme = 4000*efficiency_polm80*sigma_polm80+1000*efficiency_polp80*sigma_polp80
        S_nopol = 2500*efficiency_polm80*sigma_polm80+2500*efficiency_polp80*sigma_polp80
        significance_polscheme = S_polscheme/math.sqrt(S_polscheme+BG)
        significance_nopol = S_nopol/math.sqrt(S_nopol+BG_nopol)

        
        text += "{} {} {} {} {} {} {} {}\n".format(mstau,mneu, efficiency_polm80, efficiency_polp80, sigma_polm80, sigma_polp80, significance_polscheme, significance_nopol)

    f = open(table_file, "w")
    f.write(text)
    f.close()
    print("wrote significances to {}".format(table_file))
    
    return
  
def main(*args):
    #plot_three_examples()
    #polarisation = "polm80"
    #polarisation = "polp80"

    ###raw_input()
    ###    stau_type = "stau1"
    ###    stau_type = "stau2"
    stau_type = args[1]
    delphes_input = args[2]
    output_table_significances_prefix = args[3]
    analyze_all( stau_type,delphes_input, output_table_significances_prefix + stau_type + ".txt")


    #write_significances_standalone("table_stau1_all_newana.txt", "table_stau1_all_newana_fixedBG.txt","stau1")
    #write_significances_standalone("table_stau2_all_newana.txt", "table_stau2_all_newana.txt","stau2")

        
if __name__ == "__main__":
    main(*sys.argv)
  